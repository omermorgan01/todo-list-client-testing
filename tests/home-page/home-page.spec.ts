import * as homePageInfrastructure from '../../infrastructure/models/pages/home-page/home-page.map'

/// <reference path="../../support/index.d.ts"/>
/// <reference path="../../support/commands.ts"/>

let useFixture = (fixtureUsingFunction: (object) => void) => {
    cy.fixture('constants').then((constants) => {
        fixtureUsingFunction(constants);
    });
}

describe('todo tests', () => {
    let todo : {task: string};
    let todoToDelete : {task: string};

    beforeEach(() =>{
        useFixture((constants) => {
            todo = constants.todoDetails;
            todoToDelete = {task: todo.task};
            cy.get(homePageInfrastructure.todoInput).type(todo.task).typeEnter();
        });
    });

    afterEach(() => {
        if (todoToDelete.task != ""){
            cy.contains(todoToDelete.task).parent().find(homePageInfrastructure.removeIcon).click();
        }
    });

    it('should add todo task', () => {
        cy.contains(todo.task).should('exist');
    });

    it('should remove todo task', () => {
        todoToDelete.task = "";
        cy.contains(todo.task).parent().find(homePageInfrastructure.removeIcon).click();
        cy.contains(todo.task).should('not.exist');
    });

    it('should modify the task', () => {
        cy.contains(todo.task).parent().find(homePageInfrastructure.editIcon).click();
        useFixture((constants) => {
            cy.get(homePageInfrastructure.getEditor(todo.task)).clear().type(constants.newTask).typeEnter();
            todoToDelete.task = constants.newTask;
            cy.contains(constants.newTask).should('exist');
            cy.contains(todo.task).should('not.exist');
        });
    });

    it('should mark task as completed', () => {
        cy.contains(todo.task).click();
        useFixture((constants) => {
            cy.contains(todo.task).should('have.css', 'text-decoration', constants.completedCssValue);
        });
    });

    it('should mark task as incomplete', () => {
        cy.contains(todo.task).dblclick();
        useFixture((constants) => {
            cy.contains(todo.task).should('not.have.css', 'text-decoration', constants.completedCssValue);
        });
    });

    it('should change task date', () => {
        cy.getDateButton(todo.task).click();
        useFixture((constants) => {
            cy.contains(constants.dateNumberToAdd).click().type("{esc}");
            cy.getDateInput(todo.task).invoke('attr', homePageInfrastructure.dateValidatorAttribute).should('contain', 'false');
        });
    });

    it('should remove task date', () => {
        cy.getDateButton(todo.task).click();
        useFixture((constants) => {
            cy.contains(constants.dateNumberToAdd).click().type("{esc}");
            cy.getDateRemover(todo.task).click();
            cy.getDateInput(todo.task).invoke('val').should('contain', constants.defaultDate);
        });
    });
})

describe('todo-list tests', () => {
    let todoListToAdd : string;

    beforeEach(() => {
        useFixture((constants) => {
            todoListToAdd = constants.todoListToAdd;
            cy.get(homePageInfrastructure.todoListInput).type(todoListToAdd).typeEnter();
        });
    });

    it('should add todo list', () => {
        cy.get(homePageInfrastructure.showTodoListsButtton).click();
        cy.get(homePageInfrastructure.listContainer).should('contain', todoListToAdd);
    });

    it('should remove todo list', () => {
        cy.get(homePageInfrastructure.showTodoListsButtton).click();
        cy.contains(todoListToAdd)
            .parent().parent()
            .find(homePageInfrastructure.removeTodoListButton).click();
        cy.get(homePageInfrastructure.yesButton).click();
        cy.get(homePageInfrastructure.listContainer).should('not.contain', todoListToAdd);
    });

    it('should cancel todo list', () => {
        cy.get(homePageInfrastructure.showTodoListsButtton).click();
        cy.contains(todoListToAdd)
            .parent().parent()
            .find(homePageInfrastructure.removeTodoListButton).click();
        cy.get(homePageInfrastructure.noButton).click();
        cy.get(homePageInfrastructure.listContainer).should('contain', todoListToAdd);
    });
})