/// <reference types="cypress"/>

import * as homePageInfrastructure from '../infrastructure/models/pages/home-page/home-page.map'

Cypress.Commands.add('getDateInput', (task) => {
    return cy.contains(task).parent().parent()
        .find(homePageInfrastructure.dateEditor);
});

Cypress.Commands.add('getDateRemover', (task) => {
    return cy.contains(task).parent().parent()
        .find(homePageInfrastructure.dateRemover);
});

Cypress.Commands.add('getDateButton', (task) => {
    return cy.contains(task).parent().parent()
        .find(homePageInfrastructure.dateButton);
});

Cypress.Commands.add('typeEnter',{prevSubject: true} , (subject: Cypress.Chainable<JQuery>) => {
    return cy.get(subject[0]).type("{enter}");
});