/// <reference types="cypress"/>

declare global {
    namespace Cypress {
        interface Chainable {
            getDateInput(task: Object): Chainable<Element>
            getDateRemover(task: Object): Chainable<Element>
            getDateButton(task: Object): Chainable<Element>
            typeEnter(subject: Cypress.Chainable<JQuery>): Chainable<Element>
        }
    }
}

export = global;