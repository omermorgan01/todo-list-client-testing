export const todoInput = '[placeholder="Add your task here..."]';
export const removeIcon = '[data-test="removeIcon"]';
export const editIcon = '[data-test="editIcon"]';
export const dateEditor = 'input[id="date-picker-inline"]';
export const dateValidatorAttribute = 'aria-invalid';
export const dateRemover = 'button[data-test="removeDateButton"]';
export const dateButton = 'button[aria-label="change date"]';

export const todoListInput = '[placeholder="Add your task list here..."]';
export const showTodoListsButtton = 'button[data-test="openButton"]';
export const removeTodoListButton = 'svg';
export const yesButton = `button[data-test="yesBut"]`;
export const listContainer = `div[data-test="MainContainer"]`;
export const noButton = `button[data-test="noBut"]`;

export const getEditor = (task) => `input[value="${task}"]`;