import './commands.ts';
/// <reference path="./index.d.ts"/>
/// <reference path="./commands.ts"/>

let constants : object;
beforeEach(() => {
    cy.fixture('constants').then((constantsFromJson) => {
        constants = constantsFromJson
        cy.visit(constantsFromJson.clientUrl);
    })
});